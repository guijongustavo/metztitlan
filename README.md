# Analysis of the Water Quality of the Río Metztitlán

## Project Description

The primary purpose of this repository's project is to conduct an analysis of the water quality in the Río Metztitlán body of water. To provide a detailed and accurate view of the state of this natural resource, we have utilized a wide range of water quality data collected over an eight-year period, from 2012 to 2019. This data comes from three specific sites within the river, strategically selected to provide an adequate representation of the variations in water quality over time and space.

To facilitate understanding and access to the methods employed in this analysis, we have included scripts that automate the process of data evaluation. These scripts have been developed with a focus on transparency and reproducibility, meaning they can be used and adapted by other researchers, students, or anyone interested in the topic. The scripts are primarily written in Python, a programming language widely used in the scientific community. Therefore, having a basic knowledge of Python is recommended to effectively execute and understand these scripts.

In addition to providing the scripts, the project aims to promote collaboration and engagement from the academic community and the general public in the study of water quality. With this initiative, we hope to contribute to the conservation and improvement of the Río Metztitlán, promoting informed decision-making and the development of public policies based on solid scientific data.

## User Warning

It is important for the user to ensure that the data file they plan to use is correctly located in their working directory and that the path to this file is properly defined in the scripts. This is essential to avoid execution issues that may arise due to incorrect paths or missing files.

It is strongly recommended that the user has prior knowledge of Python, R, and data analysis, as these scripts require a basic understanding of these topics to be executed and modified successfully. Additionally, having Python 3.x installed on your computer is essential. If you do not have Python installed, you may choose to use Conda, a tool that facilitates the creation of working environments. However, be aware that the environment created by Conda may occupy up to 3 GB of space on your hard drive, so it is important to ensure that you have sufficient available space.

If you encounter errors during the execution of the scripts, it is advisable to carefully review the output provided by the program. Many common issues are due to simple mistakes, such as not placing the input file in the correct path or incorrectly defining the output path. It is also important to check that the paths specified in the script match the actual location of your files.

It is necessary to remember that the scripts provided in this project were designed for a very specific case study. Therefore, if you decide to use them for another project or dataset, you will need to adapt the input variables and ensure that the data is consistent with the analysis requirements. It is also crucial to verify that the character encoding format is correct to avoid errors in data reading.

This software is free and open source, meaning it is provided as-is, without any warranty. As a user, you have complete freedom to modify, add to, and improve the software according to your needs. However, the responsibility for the proper and effective use of this software rests solely with you. Be sure to review and test any changes you make to ensure that the software functions correctly in your specific context.

## Data


### Data Files

- `ResultadosCalidadDeAgua2012-2018.xlsx`: Contains water quality results from 2012 to 2018.
- `Resultados de Calidad del Agua 2019.xlsx`: Contains water quality results for 2019.

### Sampling Sites

The sampling sites considered in this analysis are:
- **TLACOTEPEC**
- **LAGUNA DE METZTITLAN 1**
- **LAGUNA DE METZTITLAN 2**

### Relevant Variables

The following 22 relevant variables were analyzed according to the CONAGUA document:

- `SAAM`: Sustancias Activas al Azul de Metileno (mg/L)
- `OD_mg/L`: Oxígeno Disuelto (mg/L)
- `COLI_TOT`: Coliformes Totales (NMP/100 mL)
- `pH_CAMPO`: Potencial de Hidrógeno (UpH)
- `TEMP_AGUA`: Temperatura del agua (°C)
- `NI_TOT`: Níquel Total (mg/L)
- `E_COLI`: Escherichia coli (NMP/100 mL)
- `HG_TOT`: Mercurio Total (mg/L)
- `PB_TOT`: Plomo Total (mg/L)
- `CD_TOT`: Cadmio Total (mg/L)
- `CR_TOT`: Cromo Total (mg/L)
- `AS_TOT`: Arsénico Total (mg/L)
- `TURBIEDAD`: Turbiedad (UNT)
- `SST`: Sólidos Suspendidos Totales (mg/L)
- `COLOR_VER`: Color Verdadero (U Pt/Co)
- `DUR_TOT`: Dureza Total (mg CaCO3/L)
- `N_TOT`: Nitrógeno Total (Cálculo) (mg/L)
- `COLI_FEC`: Coliformes Fecales (NMP/100 mL)
- `PO4_TOT`: Fosfatos Totales (mg/L)
- `N_NH3`: Nitrógeno Amoniacal (mg/L)
- `N_NO2`: Nitrógeno de Nitritos (mg/L)
- `N_NO3`: Nitrógeno de Nitratos (mg/L)

## Analysis

### Calculation of Variables with at Least 10 Data Points

Variables with at least 10 available data points were calculated and analyzed. The relevant variables for each site are:

#### TLACOTEPEC

The following variables have data available for the TLACOTEPEC site:

- Sustancias Activas al Azul de Metileno
- Oxígeno Disuelto
- Coliformes Totales
- Potencial de Hidrógeno
- Temperatura del agua
- Níquel Total
- Escherichia coli
- Mercurio Total
- Cromo Total
- Arsénico Total
- Turbiedad
- Sólidos Suspendidos Totales
- Color Verdadero
- Dureza Total
- Nitrógeno Total (Cálculo)
- Coliformes Fecales
- Nitrógeno Amoniacal
- Nitrógeno de Nitritos
- Nitrógeno de Nitratos

### Data Export

## Project Requirements

- Python 3.x
- Libraries: `numpy`, `pandas`, `matplotlib`

## Execution Instructions

1. Ensure Python 3.x is installed on your system.
2. Install the required libraries using `pip`. You may also use an environment like Conda or create your own environment.
3. Place the data files in a folder named `data` in the project directory.
4. Run any of the analysis notebooks (`*.ipynb`).
5. It is very important to be aware of which variables you will use in your analysis to ensure that these are correctly declared in the script, as well as to check the encoding, both for the variable names and the data file encoding.

# Water Quality Analysis Project

This project focuses on the analysis of water quality in various regions, using different data analysis techniques. Below is a description of the project structure and how to use the different scripts and data included.

## Project Structure

```
.
├── time-series-metztitlan.ipynb
├── boxplot-metztitlan.ipynb
├── violinplot-metztitlan.ipynb
├── res_cal_agua_2012-2018-exploratorio.ipynb
├── fisher-metztitlan.ipynb
├── data
├── fisher
│   ├── nodo-master
└── README.md
```

### Description of Directories and Files

- **boxplot-metztitlan.ipynb**: Jupyter Notebook that generates boxplots for the Metztitlán data.
- **data**: Directory containing raw and processed data files in CSV and Excel formats.
- **fisher**: Directory containing subdirectories with Python scripts and processed data using the Fisher method.
  - **nodo-master**: Subdirectories for analysis.

### Instructions for Using the Project

1. **Data Analysis**:
   - Use Jupyter Notebooks, such as `boxplot-metztitlan.ipynb`, to generate charts and perform exploratory data analysis. You may use a Conda environment for this, but it is not necessary. Once you decide to run the scripts, make sure that the input files are in the correct directory.
   - The files in the `fisher` directory contain Python scripts (`fisher_main.py`, `fisher.py`, etc.) for performing more advanced analysis using the Fisher method. It is recommended to review the documentation provided by N. Ahmad, S. Derrible, T. Eason, and H. Cabezas, 2016, ''Using Fisher information to track stability in multivariate systems'', Royal Society Open Science, 3:160582, DOI: 10.1098/rsos.160582

2. **Data Organization**:
   - Raw data and backup files are located in the `data` directory. This is the standard path for all Python notebooks.

3. **Script Execution**:
   - Run the Python notebooks using your preferred editor or IDE, such as vim, CODE, Spyder, Jupyter, etc. For R usage, also use your preferred IDE but ensure you have the libraries installed for mutual information calculations.

# Project


![Flowchart](images/diagrama_flujo_scripts.png)

---

This project was carried out for academic and research purposes for the case study of water quality in the Río Metztitlán.


